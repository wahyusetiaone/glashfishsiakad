-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2020 at 07:44 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siakad2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_nilai`
--

CREATE TABLE `history_nilai` (
  `id_history_nilai` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `id_kelas_kuliah` int(11) NOT NULL,
  `nilai_angka` int(5) NOT NULL,
  `nilai_huruf` varchar(5) NOT NULL,
  `nilai_indek` int(5) NOT NULL,
  `ips` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_nilai`
--

INSERT INTO `history_nilai` (`id_history_nilai`, `nim`, `periode`, `id_kelas_kuliah`, `nilai_angka`, `nilai_huruf`, `nilai_indek`, `ips`) VALUES
(771, 8834, '2016', 996, 80, 'A', 8, 4),
(772, 8821, '2017', 998, 75, 'B', 8, 4),
(773, 8835, '2015', 998, 70, 'B', 7, 3),
(774, 8845, '2018', 996, 90, 'A', 9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `history_pendidikan`
--

CREATE TABLE `history_pendidikan` (
  `nim` int(10) NOT NULL,
  `nik` int(16) NOT NULL,
  `jenis_pendaftaran` set('baru','pindahan','transfer','alih jenjang') NOT NULL,
  `jalur_pendaftaran` set('mandiri','beasiswa') NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `perguruan_tinggi` varchar(100) NOT NULL,
  `kode_prodi` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_pendidikan`
--

INSERT INTO `history_pendidikan` (`nim`, `nik`, `jenis_pendaftaran`, `jalur_pendaftaran`, `tanggal_masuk`, `perguruan_tinggi`, `kode_prodi`) VALUES
(8821, 11, 'baru', 'mandiri', '2017-08-20', 'stimik sinus', 4),
(8834, 22, 'transfer', 'mandiri', '2015-05-28', 'stimik wahid hasyim', 2),
(8835, 33, 'alih jenjang', 'beasiswa', '2017-03-14', 'stimik sinar nusaantara', 3),
(8854, 44, 'pindahan', 'beasiswa', '2019-07-01', 'stimik wahid hasyim', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_kuliah`
--

CREATE TABLE `kelas_kuliah` (
  `id_kelas_kuliah` int(11) NOT NULL,
  `kode_prodi` int(5) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `kode_matakuliah` int(5) NOT NULL,
  `nama_kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_kuliah`
--

INSERT INTO `kelas_kuliah` (`id_kelas_kuliah`, `kode_prodi`, `semester`, `kode_matakuliah`, `nama_kelas`) VALUES
(993, 2, '5', 154, 'kelas mala'),
(995, 4, '7', 172, 'kelas sore'),
(996, 3, '6', 152, 'kelas pagi'),
(998, 1, '3', 145, 'kelas sian');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_mahasiswa`
--

CREATE TABLE `master_mahasiswa` (
  `nik` int(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` datetime NOT NULL,
  `jenis_kelamin` set('L','P') NOT NULL,
  `agama` varchar(20) NOT NULL,
  `nama_ibu_kandung` varchar(50) NOT NULL,
  `nisn` int(25) NOT NULL,
  `npwp` int(25) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `jalan` varchar(50) NOT NULL,
  `dusun` varchar(50) NOT NULL,
  `rt` varchar(5) NOT NULL,
  `rw` varchar(5) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `hp` varchar(16) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_mahasiswa`
--

INSERT INTO `master_mahasiswa` (`nik`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `nama_ibu_kandung`, `nisn`, `npwp`, `kewarganegaraan`, `jalan`, `dusun`, `rt`, `rw`, `kelurahan`, `kecamatan`, `kode_pos`, `telepon`, `hp`, `email`) VALUES
(11, 'pailjo', 'd', '2019-06-07 00:00:00', 'P', 'f', 'v', 13, 33, 'd', 'df', 'g', 'gh', 'hj', 'g', 'a', 44, 'd', 'fd', 'sd'),
(22, 'maulana malik', 'ponorogo', '1990-09-09 00:00:00', 'L', 'islam', 'maryam', 99876, 98356, 'indonesia', 'jl klewer 12', 'bendo', '08', '07', 'krajan', 'bantul', 9887, '021-884937', '087708870991', 'maulana@gmail.com'),
(33, 'darsono', 'klaten', '1985-04-19 00:00:00', 'L', 'islam', 'watik', 987654, 98327, 'indonesia', 'wahid hasyim', 'bendo', '02', '05', 'juwiring', 'juwiran', 9765, '021-8876545', '084536221', 'darso@gmail.com'),
(44, 'paulus jatinegara', 'sleman', '1993-07-23 00:00:00', 'L', 'kristen', 'catrina', 786373, 627368, 'indonesia', 'wamena no 13', 'kukuh', '09', '01', 'kaligede', 'kalideres', 98007, '021-88493', '0987534744132', 'paulus@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `master_matakuliah`
--

CREATE TABLE `master_matakuliah` (
  `kode_matakuliah` int(5) NOT NULL,
  `nama_matakuliah` varchar(100) NOT NULL,
  `kode_prodi` int(5) NOT NULL,
  `jenis_matakuliah` set('wajib','pilihan') NOT NULL,
  `bobot_matakuliah` int(2) NOT NULL,
  `bobot_teori` int(2) NOT NULL,
  `bobot_praktikum` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_matakuliah`
--

INSERT INTO `master_matakuliah` (`kode_matakuliah`, `nama_matakuliah`, `kode_prodi`, `jenis_matakuliah`, `bobot_matakuliah`, `bobot_teori`, `bobot_praktikum`) VALUES
(125, 'bahasa cina', 1, 'wajib', 4, 4, 0),
(145, 'bahas indonesia', 2, 'wajib', 3, 3, 0),
(152, 'pengolahan citra', 3, 'pilihan', 4, 2, 2),
(154, 'desain web', 4, 'wajib', 4, 2, 2),
(166, 'bahasa inggris', 3, 'wajib', 3, 3, 0),
(172, 'bahasa arab', 1, 'wajib', 4, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_prodi`
--

CREATE TABLE `master_prodi` (
  `kode_prodi` int(5) NOT NULL,
  `nama_prodi` varchar(50) NOT NULL,
  `akreditasi` varchar(3) NOT NULL,
  `no_sk_prodi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_prodi`
--

INSERT INTO `master_prodi` (`kode_prodi`, `nama_prodi`, `akreditasi`, `no_sk_prodi`) VALUES
(1, 'sistem informatika', 'A', '883'),
(2, 'Computer Science', 'A', '885'),
(3, 'teknologi informatika', 'A', '881'),
(4, 'komputerisasi akuntasi', 'B', '882');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_nilai`
--
ALTER TABLE `history_nilai`
  ADD PRIMARY KEY (`id_history_nilai`);

--
-- Indexes for table `history_pendidikan`
--
ALTER TABLE `history_pendidikan`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `kelas_kuliah`
--
ALTER TABLE `kelas_kuliah`
  ADD PRIMARY KEY (`id_kelas_kuliah`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user_name`);

--
-- Indexes for table `master_mahasiswa`
--
ALTER TABLE `master_mahasiswa`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `master_matakuliah`
--
ALTER TABLE `master_matakuliah`
  ADD PRIMARY KEY (`kode_matakuliah`);

--
-- Indexes for table `master_prodi`
--
ALTER TABLE `master_prodi`
  ADD PRIMARY KEY (`kode_prodi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
