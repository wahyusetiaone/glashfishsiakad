/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.Kelas_Kuliah;
import model.Master_Prodi;
/**
 *
 * @author hanip
 */
@WebService(serviceName = "Kelas")
public class Kelas {

    Connection conn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "tampil_Kelaskuliah")
    public List<Kelas_Kuliah> tampilKelaskuliah(){
        List<Kelas_Kuliah> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from  kelas_kuliah kk inner join "
                                    + "(master_matakuliah mm inner join master_prodi mp on mm.kode_prodi = mp.kode_prodi)\n" 
                                    + "on kk.kode_matakuliah = mm.kode_matakuliah");
        rs=pstm.executeQuery();
        while(rs.next()){
        
        Kelas_Kuliah b=new Kelas_Kuliah();
        b.setId_kelaskuliah(rs.getInt("id_kelas_kuliah"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setAkreditasi(rs.getString("akreditasi")); 
        b.setSemester(rs.getString("semester"));
        b.setKode_matakuliah(rs.getInt("kode_matakuliah"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setNama_kelas(rs.getString("nama_kelas"));
        
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
    
    @WebMethod(operationName = "get_Kelaskuliah")
    public List getKelaskuliah(@WebParam(name = "id_kelas_kuliah") int
    id_kelas_kuliah){
        List mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from  kelas_kuliah kk inner join "
                                    + "(master_matakuliah mm inner join master_prodi mp on mm.kode_prodi = mp.kode_prodi)\n" 
                                    + "on kk.kode_matakuliah = mm.kode_matakuliah where kk.id_kelas_kuliah=?");
        pstm.setInt(1, id_kelas_kuliah);
        rs=pstm.executeQuery();
        while (rs.next()){
        mtk.add(rs.getInt("id_kelas_kuliah"));
        mtk.add(rs.getInt("kode_prodi"));
        mtk.add(rs.getString("nama_prodi"));
        mtk.add(rs.getString("akreditasi"));
        mtk.add(rs.getString("semester"));
        mtk.add(rs.getInt("kode_matakuliah"));
        mtk.add(rs.getString("nama_matakuliah"));
        mtk.add(rs.getString("nama_kelas"));
        
       
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    @WebMethod(operationName = "hapus_Kelaskuliah")
    public void hapusKelaskuliah(@WebParam(name = "id_kelas_kuliah") int
    id_kelas_kuliah) {
        
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("delete from kelas_kuliah where id_kelas_kuliah=?");
            pstm.setInt(1, id_kelas_kuliah);
            pstm.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    @WebMethod(operationName = "tambah_Kelaskuliah")
    public void addKelaskuliah (
        @WebParam(name = "id_kelas_kuliah") int id_kelas_kuliah,
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "semester") String semester, 
        @WebParam(name = "kode_matakuliah") int kode_matakuliah,
        @WebParam(name = "nama_kelas") String nama_kelas 
) 
    { 
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("insert into kelas_kuliah value (?,?,?,?,?)");
            pstm.setInt(1, id_kelas_kuliah);
            pstm.setInt(2, kode_prodi);
            pstm.setString(3, semester);
            pstm.setInt(4, kode_matakuliah);
            pstm.setString(5, nama_kelas);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "edit_Kelaskuliah")
    public void editKelaskuliah(
        @WebParam(name = "id_kelas_kuliah") int id_kelas_kuliah,
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "semester") String semester, 
        @WebParam(name = "kode_matakuliah") int kode_matakuliah,
        @WebParam(name = "nama_kelas") String nama_kelas 
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("update kelas_kuliah set kode_prodi=?,semester=?,kode_matakuliah=?,nama_kelas=? where id_kelas_kuliah=?");
            pstm.setInt(5, id_kelas_kuliah);
            pstm.setInt(1, kode_prodi);
            pstm.setString(2, semester);
            pstm.setInt(3, kode_matakuliah);
            pstm.setString(4, nama_kelas);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Kelaskuliah")
    public List<Kelas_Kuliah> cariKelaskuliah(@WebParam(name = "key") int key,@WebParam(name = "keys") String keys){
        List<Kelas_Kuliah> mtk = new ArrayList<>();
        keys = "%"+keys+"%";
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from kelas_kuliah where id_kelas_kuliah like ? or kode_prodi like ? or semester like ? or kode_matakuliah like ? or nama_kelas like ?");
        pstm.setInt(1, key);
        pstm.setInt(2, key);
        pstm.setString(3, keys);
        pstm.setInt(4, key);
        pstm.setString(5, keys);
       
        
       
        rs=pstm.executeQuery();
        while (rs.next()){
        Kelas_Kuliah b=new Kelas_Kuliah();
        b.setId_kelaskuliah(rs.getInt("id_kelas_kuliah"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setSemester(rs.getString("semester"));
        b.setKode_matakuliah(rs.getInt("kode_matakuliah"));
        b.setNama_kelas(rs.getString("nama_kelas"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    
     @WebMethod(operationName = "query_Kelaskuliah")
    public List<Kelas_Kuliah> queryKelaskuliah(
            @WebParam(name = "kode_prodi") String in_kode_prodi,
            @WebParam(name = "semester") String in_semester
    ){
        List<Kelas_Kuliah> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from  kelas_kuliah kk inner join "
                                    + "(master_matakuliah mm inner join master_prodi mp on mm.kode_prodi = mp.kode_prodi)\n" 
                                    + "on kk.kode_matakuliah = mm.kode_matakuliah where mp.kode_prodi "
                                    + "like CONCAT( '%',?,'%') and kk.semester like CONCAT( '%',?,'%')");
        if(in_kode_prodi.equals("all")){
            in_kode_prodi = "";
        }
        if(in_semester.equals("all")){  
            in_semester = "";
        }
        pstm.setString(1, in_kode_prodi);
        pstm.setString(2, in_semester);
        rs=pstm.executeQuery();
        while(rs.next()){
        
        Kelas_Kuliah b=new Kelas_Kuliah();
        b.setId_kelaskuliah(rs.getInt("id_kelas_kuliah"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setAkreditasi(rs.getString("akreditasi")); 
        b.setSemester(rs.getString("semester"));
        b.setKode_matakuliah(rs.getInt("kode_matakuliah"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setNama_kelas(rs.getString("nama_kelas"));
        
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
}
