/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.Master_MataKuliah;
import model.Master_Prodi;



/**
 *
 * @author hanip
 */
@WebService(serviceName = "Matakuliah")
public class Matakuliah {

    Connection conn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    //karena master matakuliah sudah extends di master prodi
    @WebMethod(operationName = "tampil_Matakuliah")
    public List<Master_MataKuliah> tampilMatakuliah(){
        List<Master_MataKuliah> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_matakuliah inner join master_prodi on master_matakuliah.kode_prodi = master_prodi.kode_prodi");
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_MataKuliah b=new Master_MataKuliah();
        b.setKode_matakuliah(rs.getInt("kode_matakuliah"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setJenis_matakuliah(rs.getString("jenis_matakuliah"));
        b.setBobot_matakuliah(rs.getInt("bobot_matakuliah"));
        b.setBobot_teori(rs.getInt("bobot_teori"));
        b.setBobot_pratikum(rs.getInt("bobot_praktikum"));
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
    @WebMethod(operationName = "get_Matakuliah")
    public List getMatakuliah(@WebParam(name = "kode_matakuliah") int
    kode_matakuliah){
        List mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_matakuliah inner join master_prodi on master_matakuliah.kode_prodi = master_prodi.kode_prodi where master_matakuliah.kode_matakuliah=?");
        pstm.setInt(1, kode_matakuliah);
        rs=pstm.executeQuery();
        while (rs.next()){
        mtk.add(rs.getInt("kode_matakuliah"));
        mtk.add(rs.getString("nama_matakuliah"));
        mtk.add(rs.getInt("kode_prodi"));
        mtk.add(rs.getString("nama_prodi"));
        mtk.add(rs.getString("jenis_matakuliah"));
        mtk.add(rs.getInt("bobot_matakuliah"));
        mtk.add(rs.getInt("bobot_teori"));
        mtk.add(rs.getInt("bobot_praktikum"));
       
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    @WebMethod(operationName = "hapus_Matakuliah")
    public void hapusMatakuliah(@WebParam(name = "kode_matakuliah") int
    kode_matakuliah) {
        
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("delete from master_matakuliah where kode_matakuliah=?");
            pstm.setInt(1, kode_matakuliah);
            pstm.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    @WebMethod(operationName = "tambah_Matakuliah")
    public void addMatakuliah (
        @WebParam(name = "kode_matakuliah") int kode_matakuliah,
        @WebParam(name = "nama_matakuliah") String nama_matakuliah,
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "jenis_matakuliah") String jenis_matakuliah, 
        @WebParam(name = "bobot_matakuliah") int bobot_matakuliah,
        @WebParam(name = "bobot_teori") int bobot_teori,
        @WebParam(name = "bobot_praktikum") int bobot_praktikum   
) 
    { 
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("insert into master_matakuliah value (?,?,?,?,?,?,?)");
            pstm.setInt(1, kode_matakuliah);
            pstm.setString(2, nama_matakuliah);
            pstm.setInt(3, kode_prodi);
            pstm.setString(4, jenis_matakuliah);
            pstm.setInt(5, bobot_matakuliah);
            pstm.setInt(6, bobot_teori);
            pstm.setInt(7, bobot_praktikum);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "edit_Matakuliah")
    public void editMatakuliah(
        @WebParam(name = "kode_matakuliah") int kode_matakuliah,
        @WebParam(name = "nama_matakuliah") String nama_matakuliah,
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "jenis_matakuliah") String jenis_matakuliah, 
        @WebParam(name = "bobot_matakuliah") int bobot_matakuliah,
        @WebParam(name = "bobot_teori") int bobot_teori,
        @WebParam(name = "bobot_praktikum") int bobot_praktikum 
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("update master_matakuliah set nama_matakuliah=?,kode_prodi=?,jenis_matakuliah=?,bobot_matakuliah=?,bobot_teori=?,bobot_praktikum=? where kode_matakuliah=?");
            pstm.setInt(7, kode_matakuliah);
            pstm.setString(1, nama_matakuliah);
            pstm.setInt(2, kode_prodi);
            pstm.setString(3, jenis_matakuliah);
            pstm.setInt(4, bobot_matakuliah);
            pstm.setInt(5, bobot_teori);
            pstm.setInt(6, bobot_praktikum);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Matakuliah")
    public List<Master_MataKuliah> cariMatakuliah(@WebParam(name = "key") int key,@WebParam(name = "keys") String keys){
        List<Master_MataKuliah> mtk = new ArrayList<>();
        keys = "%"+keys+"%";
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_matakuliah where kode_matakuliah like ? or nama_matakuliah like ? or kode_prodi like ? or jenis_matakuliah like ? or bobot_matakuliah like ? or bobot_teori like ? or bobot_praktikum like ? ");
        pstm.setInt(1, key);
        pstm.setString(2, keys);
        pstm.setInt(3, key);
        pstm.setString(4, keys);
        pstm.setInt(5, key);
        pstm.setInt(6, key);
        pstm.setInt(7, key);
        
       
        rs=pstm.executeQuery();
        while (rs.next()){
        Master_MataKuliah b=new Master_MataKuliah();
        b.setKode_matakuliah(rs.getInt("kode_matakuliah"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setJenis_matakuliah(rs.getString("jenis_matakuliah"));
        b.setBobot_matakuliah(rs.getInt("bobot_matakuliah"));
        b.setBobot_teori(rs.getInt("bobot_teori"));
        b.setBobot_pratikum(rs.getInt("bobot_praktikum"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
}