/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.Master_Prodi;



/**
 *
 * @author hanip
 */
@WebService(serviceName = "Prodi")
public class Prodi {

    Connection conn=null;
    PreparedStatement a=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "tampil_Prodi")
    public List<Master_Prodi> tampilMatakuliah(){
        List<Master_Prodi> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        a=conn.prepareStatement("select * from master_prodi");
        rs=a.executeQuery();
        while(rs.next()){
        Master_Prodi b=new Master_Prodi();
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setAkreditasi(rs.getString("akreditasi"));
        b.setNo_sk_prodi(rs.getString("no_sk_prodi"));
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
    @WebMethod(operationName = "get_Prodi")
    public List getProdi(@WebParam(name = "kode_prodi") int
    kode_prodi){
        List mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        a=conn.prepareStatement("select * from master_prodi where kode_prodi=?");
        a.setInt(1, kode_prodi);
        rs=a.executeQuery();
        while (rs.next()){
        mtk.add(rs.getInt("kode_prodi"));
        mtk.add(rs.getString("nama_prodi"));
        mtk.add(rs.getString("akreditasi"));
        mtk.add(rs.getString("no_sk_prodi"));
       
       
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    @WebMethod(operationName = "hapus_Prodi")
    public void hapusProdi(@WebParam(name = "kode_prodi") int
    kode_prodi) {
        
        try {
            conn=connectSQL.getConnect();
            a=conn.prepareStatement("delete from master_prodi where kode_prodi=?");
            a.setInt(1, kode_prodi);
            a.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    @WebMethod(operationName = "tambah_Prodi")
    public void addProdi (
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "nama_prodi") String nama_prodi,
        @WebParam(name = "akreditasi") String akreditasi, 
        @WebParam(name = "no_sk_prodi") String no_sk_prodi
        
) 
    { 
        try {
            conn=connectSQL.getConnect();
            a=conn.prepareStatement("insert into master_prodi value (?,?,?,?)");
            a.setInt(1, kode_prodi);
            a.setString(2, nama_prodi);
            a.setString(3, akreditasi);
            a.setString(4, no_sk_prodi);
            a.executeUpdate();
           
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "edit_Prodi")
    public void editProdi(
        @WebParam(name = "kode_prodi") int kode_prodi,
        @WebParam(name = "nama_prodi") String nama_prodi,
        @WebParam(name = "akreditasi") String akreditasi, 
        @WebParam(name = "no_sk_prodi") String no_sk_prodi
        
    ) {
        try {
            conn=connectSQL.getConnect();
            a=conn.prepareStatement("update master_prodi set nama_prodi=?,akreditasi=?,no_sk_prodi=? where kode_prodi=?");
            a.setInt(4, kode_prodi);
            a.setString(1, nama_prodi);
            a.setString(2, akreditasi);
            a.setString(3, no_sk_prodi);
            a.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Prodi")
    public List<Master_Prodi> cariProdi(@WebParam(name = "key") int key,@WebParam(name = "keys") String keys){
        List<Master_Prodi> mtk = new ArrayList<>();
        keys = "%"+keys+"%";
        try {
        conn=connectSQL.getConnect();
        a=conn.prepareStatement("select * from master_prodi where kode_prodi like ? or nama_prodi like ? or akreditasi like ? or no_sk_prodi like ? ");
        a.setInt(1, key);
        a.setString(2, keys);
        a.setString(3, keys);
        a.setString(4, keys);
       
        
       
        rs=a.executeQuery();
        while (rs.next()){
        Master_Prodi b=new Master_Prodi();
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setAkreditasi(rs.getString("akreditasi"));
        b.setNo_sk_prodi(rs.getString("no_sk_prodi"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
}