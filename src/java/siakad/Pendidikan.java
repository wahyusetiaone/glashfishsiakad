/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.Master_Mahasiswa;

/**
 *
 * @author abah
 */
@WebService(serviceName = "Pendidikan")
public class Pendidikan {

    /**
     * This is a sample web service operation
     */
    Connection conn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "tampil_Pendidikan")
    public List<Master_Mahasiswa> tampilPendidikan(){
        List<Master_Mahasiswa> mhs = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa inner join history_pendidikan on master_mahasiswa.nik = history_pendidikan.nik");
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_Mahasiswa data=new Master_Mahasiswa();
        data.setNik(rs.getInt("nik"));
        data.setNim(rs.getInt("nim"));
        data.setNama(rs.getString("nama"));
        data.setJenis_pendaftaran(rs.getString("jenis_pendaftaran"));
        data.setJalur_pendaftaran(rs.getString("jalur_pendaftaran"));
        data.setTgl_masuk(rs.getString("tanggal_masuk"));
        data.setPerguruan_tinggi(rs.getString("perguruan_tinggi"));
        data.setKode_prodi(rs.getInt("kode_prodi"));
       
        mhs.add(data);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mhs;
    }
    
    @WebMethod(operationName = "get_Pendidikan")
    public List getPendidikan(@WebParam(name = "nim") int
    nim){
        List mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa inner join history_pendidikan on master_mahasiswa.nik = history_pendidikan.nik where history_pendidikan.nim=?");
        pstm.setInt(1, nim);
        rs=pstm.executeQuery();
        while (rs.next()){
        mtk.add(rs.getInt("nik"));
        mtk.add(rs.getInt("nim"));
        mtk.add(rs.getString("nama"));
        mtk.add(rs.getString("jenis_pendaftaran"));
        mtk.add(rs.getString("jalur_pendaftaran"));
        mtk.add(rs.getInt("tanggal_masuk"));
        mtk.add(rs.getString("perguruan_tinggi"));
        mtk.add(rs.getString("kode_prodi"));
        
       
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    @WebMethod(operationName = "hapus_Pendidikan")
    public void hapusPendidikan(@WebParam(name = "nim") int
    nim) {
        
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("delete from history_pendidikan where nim=?");
            pstm.setInt(1, nim);
            pstm.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    @WebMethod(operationName = "tambah_Pendidikan")
    public void addPendidikan (
        @WebParam(name = "nim") int nim,
        @WebParam(name = "nik") int nik,
        @WebParam(name = "jenis_pendaftaran") String jenis_pendaftaran, 
        @WebParam(name = "jalur_pendaftaran") String jalur_pendaftaran,
        @WebParam(name = "tanggal_masuk") String tanggal_masuk, 
        @WebParam(name = "perguruan_tinggi") String perguruan_tinggi, 
        @WebParam(name = "kode_prodi") int kode_prodi 
) 
    { 
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("insert into history_pendidikan value (?,?,?,?,?,?,?)");
            pstm.setInt(1, nim);
            pstm.setInt(2, nik);
            pstm.setString(3, jenis_pendaftaran);
            pstm.setString(4, jalur_pendaftaran);
            pstm.setString(5, tanggal_masuk);
            pstm.setString(6, perguruan_tinggi);
            pstm.setInt(7, kode_prodi);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "edit_Pendidikan")
    public void editPendidikan(
        @WebParam(name = "nim") int nim,
        @WebParam(name = "nik") int nik,
        @WebParam(name = "jenis_pendaftaran") String jenis_pendaftaran, 
        @WebParam(name = "jalur_pendaftaran") String jalur_pendaftaran,
        @WebParam(name = "tanggal_masuk") String tanggal_masuk, 
        @WebParam(name = "perguruan_tinggi") String perguruan_tinggi, 
        @WebParam(name = "kode_prodi") int kode_prodi 
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("update history_pendidikan set nik=?,jenis_pendaftaran=?,jalur_pendaftaran=?,tanggal_masuk=?,perguruan_tinggi=?,kode_prodi=? where nim=?");
            pstm.setInt(7, nim);
            pstm.setInt(1, nik);
            pstm.setString(2, jenis_pendaftaran);
            pstm.setString(3, jalur_pendaftaran);
            pstm.setString(4, tanggal_masuk);
            pstm.setString(5, perguruan_tinggi);
            pstm.setInt(6, kode_prodi);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Pendidikan")
    public List<Master_Mahasiswa> cariPendidikan(@WebParam(name = "key") int key,@WebParam(name = "keys") String keys){
        List<Master_Mahasiswa> mtk = new ArrayList<>();
        keys = "%"+keys+"%";
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa inner join history_pendidikan on master_mahasiswa.nik = history_pendidikan.nik"
                + " where history_pendidikan.nim like ? or history_pendidikan.nik like ? or history_pendidikan.jenis_pendaftaran like ? "
                + "or history_pendidikan.jalur_pendaftaran like ? or history_pendidikan.tanggal_masuk like ? "
                + "or history_pendidikan.perguruan_tinggi like ? or history_pendidikan.kode_prodi like ?");
            pstm.setInt(1, key);
            pstm.setInt(2, key);
            pstm.setString(3, keys);
            pstm.setString(4, keys);
            pstm.setString(5, keys);
            pstm.setString(6, keys);
            pstm.setInt(7, key);
       
        rs=pstm.executeQuery();
        while (rs.next()){
        Master_Mahasiswa b=new Master_Mahasiswa();
        b.setNik(rs.getInt("nik"));
        b.setNim(rs.getInt("nim"));
        b.setNama(rs.getString("nama"));
        b.setJenis_pendaftaran(rs.getString("jenis_pendaftaran"));
        b.setJalur_pendaftaran(rs.getString("jalur_pendaftaran"));
        b.setTgl_masuk(rs.getString("tanggal_masuk"));
        b.setPerguruan_tinggi(rs.getString("perguruan_tinggi"));
        b.setKode_prodi(rs.getInt("kode_prodi"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    
        @WebMethod(operationName = "query_Pendidikan")
    public List<Master_Mahasiswa> queryPendidikan(
    @WebParam(name = "nik") int nik
    ){
        List<Master_Mahasiswa> mhs = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa inner join "
                + "(history_pendidikan inner join master_prodi on history_pendidikan.kode_prodi = master_prodi.kode_prodi)"
                + " on master_mahasiswa.nik = history_pendidikan.nik where master_mahasiswa.nik = ?");
        pstm.setInt(1, nik);
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_Mahasiswa data=new Master_Mahasiswa();
        data.setNik(rs.getInt("nik"));
        data.setNim(rs.getInt("nim"));
        data.setNama(rs.getString("nama"));
        data.setJenis_pendaftaran(rs.getString("jenis_pendaftaran"));
        data.setJalur_pendaftaran(rs.getString("jalur_pendaftaran"));
        data.setTgl_masuk(rs.getString("tanggal_masuk"));
        data.setPerguruan_tinggi(rs.getString("perguruan_tinggi"));
        data.setKode_prodi(rs.getInt("kode_prodi"));
        data.setNama_prodi(rs.getString("nama_prodi"));
       
        mhs.add(data);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mhs;
    }
    
}
