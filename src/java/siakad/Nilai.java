/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.History_Nilai;
import model.Master_Mahasiswa;




/**
 *
 * @author hanip
 */
@WebService(serviceName = "Nilai")
public class Nilai {

    Connection conn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    //karena master matakuliah sudah extends di master prodi
    @WebMethod(operationName = "tampil_Nilai")
    public List<Master_Mahasiswa> tampilNilai(){
        List<Master_Mahasiswa> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa mm inner join\n" +
"            (history_pendidikan hp inner join\n" +
"            (history_nilai hn inner join\n" +
"            (kelas_kuliah kk inner join\n" +
"            (master_matakuliah m inner join\n" +
"            master_prodi mp on mp.kode_prodi = m.kode_prodi)\n" +
"            on kk.kode_matakuliah = m.kode_matakuliah)\n" +
"            on hn.id_kelas_kuliah = kk.id_kelas_kuliah)\n" +
"            on hp.nim = hn.nim)\n" +
"            on mm.nik = hp.nik;");
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_Mahasiswa b=new Master_Mahasiswa();
        b.setKode_prodi(rs.getInt("kode_prodi"));
        b.setNama_prodi(rs.getString("nama_prodi"));
        b.setSemester(rs.getString("semester"));
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
    
    @WebMethod(operationName = "get_Nilai")
    public List getNilai(@WebParam(name = "id_history_nilai") int
    id_history_nilai){
        List mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa mm inner join\n" +
"            (history_pendidikan hp inner join\n" +
"            (history_nilai hn inner join\n" +
"            (kelas_kuliah kk inner join\n" +
"            (master_matakuliah m inner join\n" +
"            master_prodi mp on mp.kode_prodi = m.kode_prodi)\n" +
"            on kk.kode_matakuliah = m.kode_matakuliah)\n" +
"            on hn.id_kelas_kuliah = kk.id_kelas_kuliah)\n" +
"            on hp.nim = hn.nim)\n" +
"            on mm.nik = hp.nik where hn.id_history_nilai=?");
        pstm.setInt(1, id_history_nilai);
        rs=pstm.executeQuery();
        while (rs.next()){
        mtk.add(rs.getInt("id_history_nilai"));
        mtk.add(rs.getInt("nim"));
        mtk.add(rs.getString("periode"));
        mtk.add(rs.getInt("id_kelas_kuliah"));
        mtk.add(rs.getInt("nilai_angka"));
        mtk.add(rs.getString("nilai_huruf"));
        mtk.add(rs.getInt("nilai_indek"));
        mtk.add(rs.getInt("ips"));
       
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    @WebMethod(operationName = "hapus_Nilai")
    public void hapusNilai(@WebParam(name = "id_history_nilai") int
    id_history_nilai) {
        
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("delete from history_nilai where id_history_nilai=?");
            pstm.setInt(1, id_history_nilai);
            pstm.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    @WebMethod(operationName = "tambah_Nilai")
    public void addNilai (
        @WebParam(name = "id_history_nilai") int id_history_nilai,
        @WebParam(name = "nim") int nim,
        @WebParam(name = "periode") String periode,
        @WebParam(name = "id_kelas_kuliah") int id_kelas_kuliah,
        @WebParam(name = "nilai_angka") int nilai_angka, 
        @WebParam(name = "nilai_huruf") String nilai_huruf,
        @WebParam(name = "nilai_indek") int nilai_index,
        @WebParam(name = "ips") int ips   
) 
    { 
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("insert into history_nilai value (?,?,?,?,?,?,?,?)");
            pstm.setInt(1, id_history_nilai);
            pstm.setInt(2, nim);
            pstm.setString(3, periode);
            pstm.setInt(4, id_kelas_kuliah);
            pstm.setInt(5, nilai_angka);
            pstm.setString(6, nilai_huruf);
            pstm.setInt(7, nilai_index);
            pstm.setInt(8, ips);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "edit_Nilai")
    public void editNilai(
        @WebParam(name = "id_history_nilai") int id_history_nilai,
        @WebParam(name = "nim") int nim,
        @WebParam(name = "periode") String periode,
        @WebParam(name = "id_kelas_kuliah") int id_kelas_kuliah,
        @WebParam(name = "nilai_angka") int nilai_angka, 
        @WebParam(name = "nilai_huruf") String nilai_huruf,
        @WebParam(name = "nilai_indek") int nilai_index,
        @WebParam(name = "ips") int ips   
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("update history_nilai set nim=?,periode=?,id_kelas_kuliah=?,nilai_angka=?,nilai_huruf=?,nilai_indek=?,ips=? where id_history_nilai=?");
            pstm.setInt(8, id_history_nilai);
            pstm.setInt(1, nim);
            pstm.setString(2, periode);
            pstm.setInt(3, id_kelas_kuliah);
            pstm.setInt(4, nilai_angka);
            pstm.setString(5, nilai_huruf);
            pstm.setInt(6, nilai_index);
            pstm.setInt(7, ips);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Nilai")
    public List<History_Nilai> cariNilai(@WebParam(name = "key") int key){
        List<History_Nilai> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from kelas_kuliah inner join\n" +
"              (history_nilai hn inner join history_pendidikan hp on hn.nim = hp.nim)\n" +
"                on kelas_kuliah.id_kelas_kuliah = hn.id_kelas_kuliah where hp.nik like ? ");
        pstm.setInt(1, key);
       
        rs=pstm.executeQuery();
        while (rs.next()){
        History_Nilai b=new History_Nilai();
        b.setId_history_nilai(rs.getInt("id_history_nilai"));
        b.setNim(rs.getInt("nim"));
        b.setSemester(rs.getString("semester"));
        b.setPeriode(rs.getString("periode"));
        b.setId_kelas_kuliah(rs.getInt("id_kelas_kuliah"));
        b.setNilai_angka(rs.getInt("nilai_angka"));
        b.setNilai_huruf(rs.getString("nilai_huruf"));
        b.setNilai_indek(rs.getInt("nilai_indek"));
        b.setIps(rs.getInt("ips"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
        }
    
    
    @WebMethod(operationName = "query_Nilai")
    public List<Master_Mahasiswa> queryTampil(
            @WebParam(name = "kode_prodi") String in_kode_prodi,
            @WebParam(name = "semester") String in_semester
    ){
        List<Master_Mahasiswa> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa mm inner join\n" +
"            (history_pendidikan hp inner join\n" +
"            (history_nilai hn inner join\n" +
"            (kelas_kuliah kk inner join\n" +
"            (master_matakuliah m inner join\n" +
"            master_prodi mp on mp.kode_prodi = m.kode_prodi)\n" +
"            on kk.kode_matakuliah = m.kode_matakuliah)\n" +
"            on hn.id_kelas_kuliah = kk.id_kelas_kuliah)\n" +
"            on hp.nim = hn.nim)\n" +
"            on mm.nik = hp.nik where mp.kode_prodi like CONCAT( '%',?,'%') and kk.semester like CONCAT( '%',?,'%')");
        
        if(in_kode_prodi.equals("all")){
            in_kode_prodi = "";
        }
        if(in_semester.equals("all")){  
            in_semester = "";
        }
        pstm.setString(1, in_kode_prodi);
        pstm.setString(2, in_semester);
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_Mahasiswa b=new Master_Mahasiswa();
        b.setId_history_nilai(rs.getInt("id_history_nilai"));
        b.setNim(rs.getInt("nim"));
        b.setNama(rs.getString("nama"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setNilai_angka(rs.getInt("nilai_angka"));
        b.setNilai_huruf(rs.getString("nilai_huruf"));
        b.setIps(rs.getInt("ips"));
  
        mtk.add(b);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    } 
    
    @WebMethod(operationName = "query_CariNilai")
    public List<History_Nilai> queryCariNilai(
            @WebParam(name = "nim") int nim, 
            @WebParam(name = "semester") String semester
    ){
        List<History_Nilai> mtk = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa mm inner join\n" +
"            (history_pendidikan hp inner join\n" +
"            (history_nilai hn inner join\n" +
"            (kelas_kuliah kk inner join\n" +
"            (master_matakuliah m inner join\n" +
"            master_prodi mp on mp.kode_prodi = m.kode_prodi)\n" +
"            on kk.kode_matakuliah = m.kode_matakuliah)\n" +
"            on hn.id_kelas_kuliah = kk.id_kelas_kuliah)\n" +
"            on hp.nim = hn.nim)\n" +
"            on mm.nik = hp.nik where hp.nim like ? AND kk.semester like CONCAT( '%',?,'%') ");
        pstm.setInt(1, nim);
        pstm.setString(2, semester);
       
        rs=pstm.executeQuery();
        while (rs.next()){
        History_Nilai b=new History_Nilai();
        b.setId_history_nilai(rs.getInt("id_history_nilai"));
        b.setNim(rs.getInt("nim"));
        b.setSemester(rs.getString("semester"));
        b.setPeriode(rs.getString("periode"));
        b.setNama_matakuliah(rs.getString("nama_matakuliah"));
        b.setId_kelas_kuliah(rs.getInt("id_kelas_kuliah"));
        b.setNilai_angka(rs.getInt("nilai_angka"));
        b.setNilai_huruf(rs.getString("nilai_huruf"));
        b.setNilai_indek(rs.getInt("nilai_indek"));
        b.setIps(rs.getInt("ips"));
        mtk.add(b);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mtk;
    
    }
}