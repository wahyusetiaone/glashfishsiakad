/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siakad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import koneksi.Connect_Siakad;
import model.Master_Mahasiswa;

/**
 *
 * @author ARWS
 */
@WebService(serviceName = "Master_Mahasiswa")
public class Mahasiswa {

    Connection conn=null;
    PreparedStatement pstm=null;
    ResultSet rs=null;
    Connect_Siakad connectSQL=new Connect_Siakad();
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "tampil_Mahasiswa")
    public List<Master_Mahasiswa> tampilMahasiswa(){
        List<Master_Mahasiswa> mhs = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa");
        rs=pstm.executeQuery();
        while(rs.next()){
        Master_Mahasiswa data=new Master_Mahasiswa();
        data.setNik(rs.getInt("nik"));
        data.setNama(rs.getString("nama"));
        data.setTempat_lahir(rs.getString("tempat_lahir"));
        data.setTanggal_lahir(rs.getString("tanggal_lahir"));
        data.setJenis_kelamin(rs.getString("jenis_kelamin"));
        data.setAgama(rs.getString("agama"));
        data.setNama_ibu_kandung(rs.getString("nama_ibu_kandung"));
        data.setNisn(rs.getInt("nisn"));
        data.setNpwp(rs.getInt("npwp"));
        data.setKewarganegaraan(rs.getString("kewarganegaraan"));
        data.setJalan(rs.getString("jalan"));
        data.setDusun(rs.getString("dusun"));
        data.setRt(rs.getString("rt"));
        data.setRw(rs.getString("rw"));
        data.setKelurahan(rs.getString("kelurahan"));
        data.setKecamatan(rs.getString("kecamatan"));
        data.setKode_pos(rs.getInt("kode_pos"));
        data.setTelepon(rs.getString("telepon"));
        data.setHp(rs.getString("hp"));
        data.setEmail(rs.getString("email"));
       
        mhs.add(data);
        }
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mhs;
    }
    @WebMethod(operationName = "get_Mahasiswa")
    public List getMahasiswa(@WebParam(name = "nik") int
    nik){
        List mhs = new ArrayList<>();
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa inner join history_pendidikan on master_mahasiswa.nik = history_pendidikan.nik where history_pendidikan.nik=?");
        pstm.setInt(1, nik);
        rs=pstm.executeQuery();
        while (rs.next()){
        mhs.add(rs.getInt("nik"));
        mhs.add(rs.getString("nama"));
        mhs.add(rs.getString("tempat_lahir"));
        mhs.add(rs.getString("tanggal_lahir"));
        mhs.add(rs.getString("jenis_kelamin"));
        mhs.add(rs.getString("agama"));
        mhs.add(rs.getString("nama_ibu_kandung"));
        mhs.add(rs.getInt("nisn"));
        mhs.add(rs.getInt("npwp"));
        mhs.add(rs.getString("kewarganegaraan"));
        mhs.add(rs.getString("jalan"));
        mhs.add(rs.getString("dusun"));
        mhs.add(rs.getString("rt"));
        mhs.add(rs.getString("rw"));
        mhs.add(rs.getString("kelurahan"));
        mhs.add(rs.getString("kecamatan"));
        mhs.add(rs.getInt("kode_pos"));
        mhs.add(rs.getString("telepon"));
        mhs.add(rs.getString("hp"));
        mhs.add(rs.getString("email"));
        mhs.add(rs.getInt("nim"));
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mhs;
    
        }
    @WebMethod(operationName = "hapus_Mahasiswa")
    public void hapusMahasiswa(@WebParam(name = "nik") int
    nik) {
        
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("delete from master_mahasiswa where nik=?");
            pstm.setInt(1, nik);
            pstm.executeUpdate();
        }catch (Exception e) {
            System.out.println("Gagal Hapus: "+ e.toString());
        }
    }
    
    @WebMethod(operationName = "tambah_Mahasiswa")
    public void addMahasiswa(
        @WebParam(name = "nik") int nik,
        @WebParam(name = "nama") String nama,
        @WebParam(name = "tempat_lahir") String tempat_lahir,
        @WebParam(name = "tanggal_lahir") String tanggal_lahir, 
        @WebParam(name = "jenis_kelamin") String jenis_kelamin,
        @WebParam(name = "agama") String agama,
        @WebParam(name = "nama_ibu_kandung")String nama_ibu_kandung,
        @WebParam(name = "nisn") int nisn,
        @WebParam(name = "npwp") int npwp,
        @WebParam(name = "kewarganegaraan") String kewarganegaraan,
        @WebParam(name = "jalan") String jalan,
        @WebParam(name = "dusun") String dusun,
        @WebParam(name = "rt") String rt,
        @WebParam(name = "rw") String rw,
        @WebParam(name = "kelurahan") String kelurahan,
        @WebParam(name = "kecamatan") String kecamatan,
        @WebParam(name = "kode_pos") int kode_pos,
        @WebParam(name = "telepon") String telepon,
        @WebParam(name = "hp") String hp,
        @WebParam(name = "email") String email
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("insert into master_mahasiswa value (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            pstm.setInt(1, nik);
            pstm.setString(2, nama);
            pstm.setString(3, tempat_lahir);
            pstm.setString(4, tanggal_lahir);
            pstm.setString(5, jenis_kelamin);
            pstm.setString(6, agama);
            pstm.setString(7, nama_ibu_kandung);
            pstm.setInt(8, nisn);
            pstm.setInt(9, npwp);
            pstm.setString(10, kewarganegaraan);
            pstm.setString(11,jalan );
            pstm.setString(12,dusun );
            pstm.setString(13,rt );
            pstm.setString(14,rw);
            pstm.setString(15,kelurahan);
            pstm.setString(16,kecamatan);
            pstm.setInt(17,kode_pos);
            pstm.setString(18,telepon);
            pstm.setString(19,hp);
            pstm.setString(20,email);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
@WebMethod(operationName = "edit_Mahasiswa")
    public void editMahasiswa(
        @WebParam(name = "nik") int nik,
        @WebParam(name = "nama") String nama,
        @WebParam(name = "tempat_lahir") String tempat_lahir,
        @WebParam(name = "tanggal_lahir") String tanggal_lahir, 
        @WebParam(name = "jenis_kelamin") String jenis_kelamin,
        @WebParam(name = "agama") String agama,
        @WebParam(name = "nama_ibu_kandung")String nama_ibu_kandung,
        @WebParam(name = "nisn") int nisn,
        @WebParam(name = "npwp") int npwp,
        @WebParam(name = "kewarganegaraan") String kewarganegaraan,
        @WebParam(name = "jalan") String jalan,
        @WebParam(name = "dusun") String dusun,
        @WebParam(name = "rt") String rt,
        @WebParam(name = "rw") String rw,
        @WebParam(name = "kelurahan") String kelurahan,
        @WebParam(name = "kecamatan") String kecamatan,
        @WebParam(name = "kode_pos") int kode_pos,
        @WebParam(name = "telepon") String telepon,
        @WebParam(name = "hp") String hp,
        @WebParam(name = "email") String email
    ) {
        try {
            conn=connectSQL.getConnect();
            pstm=conn.prepareStatement("update master_mahasiswa set nama=?,tempat_lahir=?,tanggal_lahir=?,jenis_kelamin=?,agama=?,nama_ibu_kandung=?,nisn=?,npwp=?,kewarganegaraan=?,jalan=?,dusun=?,rt=?,rw=?,kelurahan=?,kecamatan=?,kode_pos=?,telepon=?,hp=?,email=? where nik=?");
            pstm.setInt(20, nik);
            pstm.setString(1, nama);
            pstm.setString(2, tempat_lahir);
            pstm.setString(3, tanggal_lahir);
            pstm.setString(4, jenis_kelamin);
            pstm.setString(5, agama);
            pstm.setString(6, nama_ibu_kandung);
            pstm.setInt(7, nisn);
            pstm.setInt(8, npwp);
            pstm.setString(9, kewarganegaraan);
            pstm.setString(10,jalan );
            pstm.setString(11,dusun );
            pstm.setString(12,rt );
            pstm.setString(13,rw);
            pstm.setString(14,kelurahan);
            pstm.setString(15,kecamatan);
            pstm.setInt(16,kode_pos);
            pstm.setString(17,telepon);
            pstm.setString(18,hp);
            pstm.setString(19,email);
            pstm.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @WebMethod(operationName = "cari_Mahasiswa")
    public List<Master_Mahasiswa> cariMahasiswa(@WebParam(name = "key") int key,@WebParam(name = "keys") String keys){
        List<Master_Mahasiswa> mhs = new ArrayList<>();
        keys = "%"+keys+"%";
        try {
        conn=connectSQL.getConnect();
        pstm=conn.prepareStatement("select * from master_mahasiswa where nik like ? or nama like ? or tempat_lahir like ? or tanggal_lahir like ? or jenis_kelamin like ? or agama like ? or nama_ibu_kandung like ? or nisn like ? or npwp like ? or kewarganegaraan like ? or jalan like ? or dusun like ? or rt like ? or rw like ? or kelurahan like ? or kecamatan like ? or kode_pos like ? or telepon like ? or hp like ? or email like ?");
        pstm.setInt(1, key);
        pstm.setString(2, keys);
        pstm.setString(3, keys);
        pstm.setString(4, keys);
        pstm.setString(5, keys);
        pstm.setInt(6, key);
        pstm.setInt(7, key);
        pstm.setString(8, keys);
        pstm.setString(9, keys);
        pstm.setString(10, keys);
        pstm.setString(11, keys);
        pstm.setString(12, keys);
        pstm.setString(13, keys);
        pstm.setString(14, keys);
        pstm.setInt(15, key);
        pstm.setString(16, keys);
        pstm.setString(17, keys);
        pstm.setString(18, keys);
        pstm.setString(19, keys);
        pstm.setString(20, keys);
       
        rs=pstm.executeQuery();
        while (rs.next()){
        Master_Mahasiswa data=new Master_Mahasiswa();
        data.setNik(rs.getInt("nik"));
        data.setNama(rs.getString("nama"));
        data.setTempat_lahir(rs.getString("tempat_lahir"));
        data.setTanggal_lahir(rs.getString("tanggal_lahir"));
        data.setJenis_kelamin(rs.getString("jenis_kelamin"));
        data.setAgama(rs.getString("agama"));
        data.setNama_ibu_kandung(rs.getString("nama_ibu_kandung"));
        data.setNisn(rs.getInt("nisn"));
        data.setNpwp(rs.getInt("npwp"));
        data.setKewarganegaraan(rs.getString("kewarganegaraan"));
        data.setJalan(rs.getString("jalan"));
        data.setDusun(rs.getString("dusun"));
        data.setRt(rs.getString("rt"));
        data.setRw(rs.getString("rw"));
        data.setKelurahan(rs.getString("kelurahan"));
        data.setKecamatan(rs.getString("kecamatan"));
        data.setKode_pos(rs.getInt("kode_pos"));
        data.setTelepon(rs.getString("telepon"));
        data.setHp(rs.getString("hp"));
        data.setEmail(rs.getString("email"));
       
        mhs.add(data);
        }
        
        }catch (Exception e){
        System.out.println("Gagal Tampil :"+e.toString());
        }return mhs;
    
        }
}
