/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ARWS
 */
public class Master_Prodi {
 
    private int kode_prodi;
    private String nama_prodi, akreditasi, no_sk_prodi;

    public int getKode_prodi() {
        return kode_prodi;
    }

    public void setKode_prodi(int kode_prodi) {
        this.kode_prodi = kode_prodi;
    }

    public String getNama_prodi() {
        return nama_prodi;
    }

    public void setNama_prodi(String nama_prodi) {
        this.nama_prodi = nama_prodi;
    }

    public String getAkreditasi() {
        return akreditasi;
    }

    public void setAkreditasi(String akreditasi) {
        this.akreditasi = akreditasi;
    }

    public String getNo_sk_prodi() {
        return no_sk_prodi;
    }

    public void setNo_sk_prodi(String no_sk_prodi) {
        this.no_sk_prodi = no_sk_prodi;
    }
    
    
    
}
