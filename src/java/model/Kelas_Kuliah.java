/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ARWS
 */
public class Kelas_Kuliah extends Master_MataKuliah {
    
    private int id_kelaskuliah, kode_prodi, kode_matakuliah;
    private String semester, nama_kelas;

    public int getId_kelaskuliah() {
        return id_kelaskuliah;
    }

    public void setId_kelaskuliah(int id_kelaskuliah) {
        this.id_kelaskuliah = id_kelaskuliah;
    }

    public int getKode_prodi() {
        return kode_prodi;
    }

    public void setKode_prodi(int kode_prodi) {
        this.kode_prodi = kode_prodi;
    }

    public int getKode_matakuliah() {
        return kode_matakuliah;
    }

    public void setKode_matakuliah(int kode_matakuliah) {
        this.kode_matakuliah = kode_matakuliah;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getNama_kelas() {
        return nama_kelas;
    }

    public void setNama_kelas(String nama_kelas) {
        this.nama_kelas = nama_kelas;
    }
    
    
    
}
