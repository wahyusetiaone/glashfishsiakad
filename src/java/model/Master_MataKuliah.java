/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ARWS
 */
public class Master_MataKuliah extends Master_Prodi{
    
    private int kode_matakuliah, kode_prodi, bobot_matakuliah, bobot_teori, bobot_pratikum;
    private String nama_matakuliah, jenis_matakuliah;
    
    private String KEY_WAJIB ="wajib";
    private String KEY_PILIHAN ="pilihan";

    public int getKode_matakuliah() {
        return kode_matakuliah;
    }

    public void setKode_matakuliah(int kode_matakuliah) {
        this.kode_matakuliah = kode_matakuliah;
    }

    public int getKode_prodi() {
        return kode_prodi;
    }

    public void setKode_prodi(int kode_prodi) {
        this.kode_prodi = kode_prodi;
    }

    public int getBobot_matakuliah() {
        return bobot_matakuliah;
    }

    public void setBobot_matakuliah(int bobot_matakuliah) {
        this.bobot_matakuliah = bobot_matakuliah;
    }

    public int getBobot_teori() {
        return bobot_teori;
    }

    public void setBobot_teori(int bobot_teori) {
        this.bobot_teori = bobot_teori;
    }

    public int getBobot_pratikum() {
        return bobot_pratikum;
    }

    public void setBobot_pratikum(int bobot_pratikum) {
        this.bobot_pratikum = bobot_pratikum;
    }

    public String getNama_matakuliah() {
        return nama_matakuliah;
    }

    public void setNama_matakuliah(String nama_matakuliah) {
        this.nama_matakuliah = nama_matakuliah;
    }

    public String getJenis_matakuliah() {
        return jenis_matakuliah;
    }

    public void setJenis_matakuliah(String jenis_matakuliah) {
        this.jenis_matakuliah = jenis_matakuliah;
    }
    
    
    
}
