/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ARWS
 */
public class History_Nilai extends Kelas_Kuliah {
    
    private int id_history_nilai, nim, id_kelas_kuliah, nilai_angka, nilai_indek, ips;
    private String periode, nilai_huruf;

    public int getId_history_nilai() {
        return id_history_nilai;
    }

    public void setId_history_nilai(int id_history_nilai) {
        this.id_history_nilai = id_history_nilai;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public int getId_kelas_kuliah() {
        return id_kelas_kuliah;
    }

    public void setId_kelas_kuliah(int id_kelas_kuliah) {
        this.id_kelas_kuliah = id_kelas_kuliah;
    }

    public int getNilai_angka() {
        return nilai_angka;
    }

    public void setNilai_angka(int nilai_angka) {
        this.nilai_angka = nilai_angka;
    }

    public int getNilai_indek() {
        return nilai_indek;
    }

    public void setNilai_indek(int nilai_indek) {
        this.nilai_indek = nilai_indek;
    }

    public int getIps() {
        return ips;
    }

    public void setIps(int ips) {
        this.ips = ips;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getNilai_huruf() {
        return nilai_huruf;
    }

    public void setNilai_huruf(String nilai_huruf) {
        this.nilai_huruf = nilai_huruf;
    }
    
    
    
}
