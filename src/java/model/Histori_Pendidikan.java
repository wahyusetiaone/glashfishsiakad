/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author ARWS
 */
public class Histori_Pendidikan extends History_Nilai {
    
    private int nim, nik, kode_prodi;
    private String perguruan_tinggi, jenis_pendaftaran, jalur_pendaftaran, tgl_masuk;
    
    private String KEY_BARU ="baru";
    private String KEY_PINDAH ="pindah";
    private String KEY_TRANSFER ="transfer";
    private String KEY_ALIH_JENJANG ="alih jenjang";
    private String KEY_MANDIRI ="mandiri";
    private String KEY_BEASISWA ="beasiswa";

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public int getNik() {
        return nik;
    }

    public void setNik(int nik) {
        this.nik = nik;
    }

    public int getKode_prodi() {
        return kode_prodi;
    }

    public void setKode_prodi(int kode_prodi) {
        this.kode_prodi = kode_prodi;
    }

    public String getPerguruan_tinggi() {
        return perguruan_tinggi;
    }

    public void setPerguruan_tinggi(String perguruan_tinggi) {
        this.perguruan_tinggi = perguruan_tinggi;
    }

    public String getJenis_pendaftaran() {
        return jenis_pendaftaran;
    }

    public void setJenis_pendaftaran(String jenis_pendaftaran) {
        this.jenis_pendaftaran = jenis_pendaftaran;
    }

    public String getJalur_pendaftaran() {
        return jalur_pendaftaran;
    }

    public void setJalur_pendaftaran(String jalur_pendaftaran) {
        this.jalur_pendaftaran = jalur_pendaftaran;
    }

    public String getTgl_masuk() {
        return tgl_masuk;
    }

    public void setTgl_masuk(String tgl_masuk) {
        this.tgl_masuk = tgl_masuk;
    }
    
    
}
